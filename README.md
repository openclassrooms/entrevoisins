# EntreVoisins

**EntreVoisins** est le projet 3 du parcours DA Android 
d'OpenClassrooms.  

## Introduction

**EntreVoisins** est une application qui permet aux personnes d'un même 
quartier de se rendre des services (garde d'animaux, bricolage, troc, 
cours particuliers ...).  

Dans ce projet nous ne travaillons que sur la partie "gestion des 
voisins".  

## Travail à réaliser

Actuellement il est uniquement possible d'ajouter ou supprimer une 
personne dans la liste des voisins (*version 0.1*).  

Nous souhaitons maintenant pouvoir afficher plus de détails concernant 
les voisins et aussi gérer des favoris. Nous en profiterons pour mettre 
en place des tests automatisés.  

### Outils et technologies utilisés

  - **Draw.io :** dessin de diagramme en ligne
  - **Android Strudio :** Environnement de Développement Intégré (IDE)
  - **Java :** langage de développement utilisé (contrainte projet)
  - **Git :** gestionnaire de version de code source décentralisé
  - **BitBucket :** forge logiciel (dépôt de code source, bugtracker 
  ...)

### Planification du travail

Implémentation des nouvelles fonctionnalités :  

  - **v0.2 :** ajouter des informations détaillés à propos des voisins
  - **v0.3 :** gérer les favoris

**Ma planification :**  

  - analyse et conception global
  - pour chaque fonctionnalités (**v0.2** et **v0.3**) :  
    - analyse et conception
    - implémentation des tests unitaires
    - implémentation de la fonctionnalité
    - validation des tests (BugFix si nécessaire)
    - rédaction de la JavaDoc
  - test et validation global (tests instrumentalisés)

### Livrables

  - le code source de l'application contenant les nouvelles 
  fonctionnalités
  - les instructions pour pouvoir exécuter et compiler l'application 
  (dans ce fichier `README.md`)
  - les rapports d'exécution des tests unitaires et instrumentalisés 
  qui indiquent que ces derniers sont validés

## Réalisation

### Version 0.2

Ajouter des informations détaillés à propos des voisins. Voici le 
prototype de la fonctionnalité :  

![prototype de la fonctionnalité "afficher les informations d'un voisin"](EntreVoisins_v0.2.png)  

Dans le modèle il y a quatre attributs a rajouter à la classe 
`Neighbour` :  

![Diagramme de classe du modèle v0.2](model_class_diagram_v0.2.png)  

### Version 0.3

Gérer les favoris. Voici le prototype de la fonctionnalité :  

![prototype de la fonctionnalité "afficher les informations d'un voisin"](EntreVoisins_v0.3.png)  

Dans le modèle il y a un attribut a rajouter à la classe `Neighbour` :  

![Diagramme de classe du modèle v0.3](model_class_diagram_v0.3.png)  

### Tests

Implémentation des tests unitaire pour les nouvelles routes de l'API :  

  - obtenir la liste des voisins en favoris
  - mettre a jour un voisin (mettre/enlever des favoris)

Utilisation de la lib Espresso pour réaliser les tests 
instrumentalisés suivant :  

  - vérifier que la suppression d'un voisin soit bien effective
  - vérifier que lorsque l'on clique sur un voisin dans la liste on 
  affiche on lance une nouvelle activité
  - vérifier que lorsque l'on clique sur un voisin dans la liste on 
  affiche bien le détail de celui-ci dans la nouvelle activité 
  (vérification de la valeur d'un champ)
  - vérifier que lorsque l'on ajoute/enlève un voisin de nos favoris le
  changement soit effectif

## Conclusion

### Première soutenance

**Partie application très bien développée**, toutes les fonctionnalités 
demandées bien créées avec un plus : la possibilité d'ajouter/retirer 
le favori directement sur le recyclerview. **Par contre quelques 
incohérences dans les tests empêchent la validation. Projet à 
retravailler.**  

**Les points à revoir :**  

  - **tests Unitaires :** la fonction de tests `getFavoritesWithSuccess()` 
  est à refaire
  - **tests Instrumentalisés :** Les tests exigés ne sont pas complets (le 
  test de l'affichage de la page détails au click sur le voisin n'est 
  pas complet et devra être séparé du test de remplissage du textview 
  du nom sur la page détails) + l'utilisation de la méthode `sleep()` 
  qui n'est pas recommandée

### Seconde soutenance

**Partie application très bien développée**, toutes les fonctionnalités 
demandées bien créées avec un plus : la possibilité d'ajouter/retirer 
le favori directement sur le recyclerview. **L'étudiant a aussi corrigé 
les incohérences dans les tests. Projet validé.**  

**Les corrections demandées ont bien été implémentées :**  

  - **tests Unitaires :** Les tests passent en vert et la fonction de 
  tests `getFavoritesWithSuccess()` est désormais refaite convenablement
  - **tests Instrumentalisés :** Les tests exigés sont désormais 
  complets et tous indépendants - L'étudiant a complété le test de 
  l'affichage de la page détails au click sur le voisin et l'a séparé 
  du test de remplissage du textview du nom sur la page détails + La 
  méthode `sleep()` est désormais retirée

## Démarrage

### Importer le projet

Sur la page de démarrage d'Android Studio sélectionner successivement :  

  - `Check out project from Version Control`
  - Cliquer sur `Git`
  - Saisir l'url 
  `https://mikaelflora@bitbucket.org/openclassrooms/entrevoisins.git` 
  puis cliquer sur `Clone`
  - a la question `Would you like to open it?` cliquer sur `Yes`

Le projet est maintenant importé. Il ne reste plus qu'à attendre 
quelques secondes que les dépendances soient importées et que le projet 
soit compilé par Gradle.  

### Executer l'application

Cliquer sur le triangle vert `Run 'app'` (ou appuyer simultanéement sur 
les touches claviers `[Maj] + [F10]`). L'application se lance dans un 
Virtual Device. Vous pouvez maintenant tester l'application.  

P.S.: Si aucun Virtual Device n'est présent, une fenêtre vous proposera 
d'en créer un.  

## Sources

[**YouTube "Coding in Flow" -** ConstraintLayout Tutorial (PLACEHOLDERS & ANIMATIONS)](https://www.youtube.com/watch?v=LQ1DKrCYwz4)    
[**Google / Android -** Error "method not mocked"](https://sites.google.com/a/android.com/tools/tech-docs/unit-testing-support#TOC-Method-...-not-mocked.-)  
[**Android -** Test > Espresso > Intents](https://developer.android.com/reference/android/support/test/espresso/intent/Intents.html)  
