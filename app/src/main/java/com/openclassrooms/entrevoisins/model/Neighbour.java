package com.openclassrooms.entrevoisins.model;

import android.telephony.PhoneNumberUtils;

import java.io.Serializable;

import java.net.URL;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Objects;

/**
 * Model object representing a Neighbour
 */
public class Neighbour implements Serializable {

    /** Identifier */
    private Integer id;

    /** Full name */
    private String name;

    /** Avatar */
    private String avatarUrl;

    /** Phone number */
    private String phoneNumber;

    /** Social link */
    private String social;

    /** About me */
    private String aboutMe;

    /** City life */
    private String city;

    /** Favorite */
    private Boolean isFavorite;

    /**
     * Constructor
     * @param id neighbour unique identifier
     * @param name neighbour name
     * @param avatarUrl neighbour avatar url
     */
    public Neighbour(Integer id, String name, String avatarUrl) {
        this(id, name, avatarUrl, "");
    }

    /**
     * Constructor
     * @param id neighbour unique identifier
     * @param name neighbour name
     * @param avatarUrl neighbour avatar url
     * @param phoneNumber neighbour phone number
     */
    public Neighbour(Integer id, String name, String avatarUrl, String phoneNumber) {
        this(id, name, avatarUrl, phoneNumber, "");
    }

    /**
     * Constructor
     * @param id neighbour unique identifier
     * @param name neighbour name
     * @param avatarUrl neighbour avatar url
     * @param phoneNumber neighbour phone number
     * @param social neighbour social link
     */
    public Neighbour(Integer id, String name, String avatarUrl, String phoneNumber, String social) {
        this(id, name, avatarUrl, phoneNumber, social, "");
    }

    /**
     * Constructor
     * @param id neighbour unique identifier
     * @param name neighbour name
     * @param avatarUrl neighbour avatar url
     * @param phoneNumber neighbour phone number
     * @param social neighbour social link
     * @param aboutMe about neighbour
     */
    public Neighbour(Integer id, String name, String avatarUrl, String phoneNumber, String social, String aboutMe) {
        this(id, name, avatarUrl, phoneNumber, social, aboutMe, "");
    }

    /**
     * Constructor
     * @param id neighbour unique identifier
     * @param name neighbour name
     * @param avatarUrl neighbour avatar url
     * @param phoneNumber neighbour phone number
     * @param social neighbour social link
     * @param aboutMe about neighbour
     * @param city town of residence
     */
    public Neighbour(Integer id, String name, String avatarUrl, String phoneNumber, String social, String aboutMe, String city) {
        this(id, name, avatarUrl, phoneNumber, social, aboutMe, city, false);
    }

    /**
     * Constructor
     * @param id neighbour unique identifier
     * @param name neighbour name
     * @param avatarUrl neighbour avatar url
     * @param phoneNumber neighbour phone number
     * @param social neighbour social link
     * @param aboutMe about neighbour
     * @param city town of residence
     * @param isFavorite add neighbour to favorites
     */
    public Neighbour(Integer id, String name, String avatarUrl, String phoneNumber, String social, String aboutMe, String city, Boolean isFavorite) {
        this.id = id;
        this.name = name;
        this.avatarUrl = avatarUrl;
        this.setPhoneNumber(phoneNumber);
        this.social = social;
        this.aboutMe = aboutMe;
        this.city = city;
        this.isFavorite = isFavorite;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
         this.phoneNumber = PhoneNumberUtils.formatNumber(phoneNumber, "FR");
    }

    public String getSocial() {
        return social;
    }

    public void setSocial(String social) {
        try {
            new URL(social).toURI();
            this.social = social;
        } catch (URISyntaxException exception) {
        } catch (MalformedURLException exception) {
        }
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Boolean getFavorite() {
        return isFavorite;
    }

    public void setFavorite(Boolean favorite) {
        isFavorite = favorite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Neighbour neighbour = (Neighbour) o;
        return Objects.equals(id, neighbour.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
