package com.openclassrooms.entrevoisins.service;

import com.openclassrooms.entrevoisins.model.Neighbour;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class DummyNeighbourGenerator {

    public static List<Neighbour> DUMMY_NEIGHBOURS = Arrays.asList(
            new Neighbour(1,
                    "Caroline",
                    "http://i.pravatar.cc/150?u=a042581f4e29026704d",
                    "+33686579014",
                    "www.facebook.com/caroline",
                    "Bonjour ! Je souhaiterais faire de la marche nordique. Pas " +
                            "initiée, je recherche une ou plusieurs personnes susceptibles de " +
                            "m'accompagner ! J'aime les jeux de cartes tels que la belote et le " +
                            "tarot ...",
                    "Saint Pierre du mont à 5 km"),
            new Neighbour(2,
                    "Jack",
                    "http://i.pravatar.cc/150?u=a042581f4e29026704e",
                    "0650340298",
                    "https://facebook/jack.matis",
                    "J'aime le sport et l'aventure ! Je pratique le cross-fit, la " +
                            "randonnée, le parachutisme et plein d'autres choses. Si ça vous " +
                            "tente des séances ensemble n'hésitez pas à me contacter. A bientôt.",
                    "Lille"),
            new Neighbour(3,
                    "Chloé",
                    "http://i.pravatar.cc/150?u=a042581f4e29026704f",
                    "0623908892",
                    "https://facebook/chloe.taures",
                    "Je cours tout les week-ends et je ne suis pas contre des soirées " +
                            "arrosées ;)",
                    "Lille"),
            new Neighbour(4,
                    "Vincent",
                    "http://i.pravatar.cc/150?u=a042581f4e29026704a",
                    "0734990214",
                    "https://facebook/vincent.lacour",
                    "Si vous entendez la musique, c'est qu'il y a une soirée chéz moi " +
                            "et vous êtes les bienvenues ! Par contre si je ne réponds pas c'est " +
                            "que je dors, alors ... Chut !",
                    "Lille"),
            new Neighbour(5,
                    "Elodie",
                    "http://i.pravatar.cc/150?u=a042581f4e29026704b",
                    "0698390043",
                    "https://twitter.com/ElodieMelasse",
                    "J'aime les soirée raclette et jeux de sociétés alors n'hésitez pas " +
                            "à passer un soir :)",
                    "Villeneuve-d'Ascq",
                    true),
            new Neighbour(6,
                    "Sylvain",
                    "http://i.pravatar.cc/150?u=a042581f4e29026704c",
                    "0632343290",
                    "",
                    "Geek a mes heures perdu j'aime joué a Wow. Lorsqu'il m'arrive de " +
                            "sortir c'est pour aller faire les courses :)). Sinon tout les " +
                            "Samedi soir c'est jeux de société chez moi ;)",
                    "Lille",
                    true),
            new Neighbour(7,
                    "Laetitia",
                    "http://i.pravatar.cc/150?u=a042581f4e29026703d",
                    "",
                    "",
                    "",
                    "Villeneuve-d'Ascq",
                    true),
            new Neighbour(8,
                    "Dan",
                    "http://i.pravatar.cc/150?u=a042581f4e29026703b",
                    "",
                    "",
                    "",
                    "Lille"),
            new Neighbour(9,
                    "Joseph",
                    "https://i.pravatar.cc/150?u=a042581f4e29026704u"),
            new Neighbour(10,
                    "Emma",
                    "http://i.pravatar.cc/150?u=a042581f4e29026706d"),
            new Neighbour(11,
                    "Patrick",
                    "http://i.pravatar.cc/150?u=a042581f4e29026702d"),
            new Neighbour(12,
                    "Ludovic",
                    "http://i.pravatar.cc/150?u=a042581f3e39026702d")
    );

    public static List<Neighbour> FAVORITE_NEIGHBOURS = Arrays.asList(
            new Neighbour(5,
                    "Elodie",
                    "http://i.pravatar.cc/150?u=a042581f4e29026704b",
                    "0698390043",
                    "https://twitter.com/ElodieMelasse",
                    "J'aime les soirée raclette et jeux de sociétés alors n'hésitez pas " +
                            "à passer un soir :)",
                    "Villeneuve-d'Ascq",
                    true),
            new Neighbour(6,
                    "Sylvain",
                    "http://i.pravatar.cc/150?u=a042581f4e29026704c",
                    "0632343290",
                    "",
                    "Geek a mes heures perdu j'aime joué a Wow. Lorsqu'il m'arrive de " +
                            "sortir c'est pour aller faire les courses :)). Sinon tout les " +
                            "Samedi soir c'est jeux de société chez moi ;)",
                    "Lille",
                    true),
            new Neighbour(7,
                    "Laetitia",
                    "http://i.pravatar.cc/150?u=a042581f4e29026703d",
                    "",
                    "",
                    "",
                    "Villeneuve-d'Ascq",
                    true)
    );

    static List<Neighbour> generateNeighbours() {
        return new ArrayList<>(DUMMY_NEIGHBOURS);
    }
}
