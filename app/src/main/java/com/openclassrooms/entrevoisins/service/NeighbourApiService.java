package com.openclassrooms.entrevoisins.service;

import com.openclassrooms.entrevoisins.model.Neighbour;

import java.util.List;


/**
 * Neighbour API client
 */
public interface NeighbourApiService {

    /**
     * Get all my Neighbours
     * @return {@link List}
     */
    List<Neighbour> getNeighbours();

    /**
     * Get favorite Neighbours
     * @return {@link List}
     */
    List<Neighbour> getFavoriteNeighbours();

    /**
     * Deletes a neighbour
     * @param neighbour neighbour to delete
     */
    void deleteNeighbour(Neighbour neighbour);

    /**
     * Add or remove neighbour to favorite
     * @param neighbour neighbour to set or unset from favorite
     */
    void updateFavoriteNeighbour(Neighbour neighbour);
}
