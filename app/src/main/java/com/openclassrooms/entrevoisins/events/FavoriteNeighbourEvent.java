package com.openclassrooms.entrevoisins.events;

import com.openclassrooms.entrevoisins.model.Neighbour;

/**
 * Event fired when a user set to favorite a Neighbour
 */
public class FavoriteNeighbourEvent {
    /**
     * Neighbour to set or unset from favorite
     */
    public Neighbour neighbour;

    /**
     * Constructor.
     * @param neighbour neighbour to set or unset from favorite
     */
    public FavoriteNeighbourEvent(Neighbour neighbour) {
        this.neighbour = neighbour;
    }
}
