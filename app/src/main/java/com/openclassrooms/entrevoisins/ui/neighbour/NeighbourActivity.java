package com.openclassrooms.entrevoisins.ui.neighbour;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.openclassrooms.entrevoisins.R;
import com.openclassrooms.entrevoisins.model.Neighbour;

public class NeighbourActivity extends AppCompatActivity {

    public static final String BUNDLE_CHANGE_FAVORITE = "BUNDLE_IS_FAVORITE";
    public static final String BUNDLE_NEIGHBOUR = "BUNDLE_NEIGHBOUR";
    private Neighbour mNeighbour;
    private ImageView mAvatar;
    private TextView mTitle, mName, mCity, mPhoneNumber, mSocial, mAboutMe;
    private ImageButton mReturn;
    public FloatingActionButton mIsFavorite;
    private Boolean mFavoriteOri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_neighbour);

        mAvatar = findViewById(R.id.avatar_iv);
        mTitle = findViewById(R.id.title_tv);
        mName = findViewById(R.id.name_tv);
        mCity = findViewById(R.id.city_tv);
        mPhoneNumber = findViewById(R.id.phone_number_tv);
        mSocial = findViewById(R.id.social_tv);
        mAboutMe = findViewById(R.id.about_me_tv);
        mReturn = findViewById(R.id.return_ib);
        mIsFavorite = findViewById(R.id.is_favorite_fab);

        mNeighbour = (Neighbour) getIntent().getSerializableExtra("Neighbour");

        Glide.with(getApplicationContext())
                .load(mNeighbour.getAvatarUrl())
                .into(mAvatar);
        mTitle.setText(mNeighbour.getName());
        mName.setText(mNeighbour.getName());
        mCity.setText(mNeighbour.getCity());
        mPhoneNumber.setText(mNeighbour.getPhoneNumber());
        mSocial.setText(mNeighbour.getSocial());
        mAboutMe.setText(mNeighbour.getAboutMe());
        setIsFavorite();

        mFavoriteOri = mNeighbour.getFavorite();

        mReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra(BUNDLE_CHANGE_FAVORITE, (Boolean.compare(mFavoriteOri, mNeighbour.getFavorite()) != 0));

                if (Boolean.compare(mFavoriteOri, mNeighbour.getFavorite()) != 0) {
                    mNeighbour.setFavorite(!mNeighbour.getFavorite());
                    intent.putExtra(BUNDLE_NEIGHBOUR, mNeighbour);
                }

                setResult(RESULT_OK, intent);

                finish();
            }
        });

        mIsFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mNeighbour.setFavorite(!mNeighbour.getFavorite());
                setIsFavorite();
            }
        });
    }

    private void setIsFavorite() {
        if (mNeighbour.getFavorite())
            mIsFavorite.setImageResource(R.drawable.ic_star_yellow_48dp);
        else
            mIsFavorite.setImageResource(R.drawable.ic_star_border_grey_48dp);
    }
}
