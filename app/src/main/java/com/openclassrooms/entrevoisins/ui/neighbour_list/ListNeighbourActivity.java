package com.openclassrooms.entrevoisins.ui.neighbour_list;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.openclassrooms.entrevoisins.R;
import com.openclassrooms.entrevoisins.di.DI;
import com.openclassrooms.entrevoisins.events.DeleteNeighbourEvent;
import com.openclassrooms.entrevoisins.events.DisplayNeighbourEvent;
import com.openclassrooms.entrevoisins.events.FavoriteNeighbourEvent;
import com.openclassrooms.entrevoisins.model.Neighbour;
import com.openclassrooms.entrevoisins.service.NeighbourApiService;
import com.openclassrooms.entrevoisins.ui.neighbour.NeighbourActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListNeighbourActivity extends AppCompatActivity {

    private static final int NEIGHBOUR_ACTIVITY_REQUEST_CODE = 1;
    private NeighbourApiService mApiService;
    // UI Components
    @BindView(R.id.tabs)
    TabLayout mTabLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.container)
    ViewPager mViewPager;

    ListNeighbourPagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_neighbour);
        ButterKnife.bind(this);

        mApiService = DI.getNeighbourApiService();

        setSupportActionBar(mToolbar);
        mPagerAdapter = new ListNeighbourPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (NEIGHBOUR_ACTIVITY_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {
            boolean changeFavorite = data.getBooleanExtra(NeighbourActivity.BUNDLE_CHANGE_FAVORITE, false);
            if (changeFavorite) {
                mApiService.updateFavoriteNeighbour((Neighbour) data.getSerializableExtra(NeighbourActivity.BUNDLE_NEIGHBOUR));
                mApiService = DI.getNeighbourApiService();
                init();
            }
        }
    }

    /**
     * Fired if the user clicks on a delete button
     * @param event neighbour to delete
     */
    @Subscribe
    public void onDeleteNeighbour(DeleteNeighbourEvent event) {
        mApiService.deleteNeighbour(event.neighbour);
        mApiService = DI.getNeighbourApiService();
        init();
    }

    /**
     * Fired if the user clicks on a neighbour
     * @param event neighbour to display
     */
    @Subscribe
    public void onDisplayNeighbour(DisplayNeighbourEvent event) {
        Intent intent = new Intent(ListNeighbourActivity.this, NeighbourActivity.class);
        intent.putExtra("Neighbour", event.neighbour);
        startActivityForResult(intent, NEIGHBOUR_ACTIVITY_REQUEST_CODE);
    }

    /**
     * Fired if the user clicks on a favorite button
     * @param event neighbour to set/unset on favorites
     */
    @Subscribe
    public void onFavoriteNeighbour(FavoriteNeighbourEvent event) {
        mApiService.updateFavoriteNeighbour(event.neighbour);
        mApiService = DI.getNeighbourApiService();
        init();
    }

    private void init() {
        int position=mTabLayout.getSelectedTabPosition();
        setSupportActionBar(mToolbar);
        mPagerAdapter = new ListNeighbourPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        mTabLayout.setScrollPosition(position,0f,true);
        mViewPager.setCurrentItem(position);
    }
}
