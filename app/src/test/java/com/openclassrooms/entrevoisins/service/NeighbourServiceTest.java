package com.openclassrooms.entrevoisins.service;

import com.openclassrooms.entrevoisins.di.DI;
import com.openclassrooms.entrevoisins.model.Neighbour;

import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertThat;

/**
 * Unit test on Neighbour service
 */
@RunWith(JUnit4.class)
public class NeighbourServiceTest {

    // This is fixed
    private static int FAVORITE_ITEMS_COUNT = 3;

    private NeighbourApiService service;

    @Before
    public void setup() {
        service = DI.getNewInstanceApiService();
    }

    @Test
    public void getNeighboursWithSuccess() {
        List<Neighbour> neighbours = service.getNeighbours();
        List<Neighbour> expectedNeighbours = DummyNeighbourGenerator.DUMMY_NEIGHBOURS;
        assertThat(neighbours, IsIterableContainingInAnyOrder.containsInAnyOrder(expectedNeighbours.toArray()));
    }

    @Test
    public void deleteNeighbourWithSuccess() {
        Neighbour neighbourToDelete = service.getNeighbours().get(0);
        service.deleteNeighbour(neighbourToDelete);
        assertFalse(service.getNeighbours().contains(neighbourToDelete));
    }

    @Test
    public void getFavoriteNeighbourWithSuccess() {
        List<Neighbour> neighbours = service.getFavoriteNeighbours();
        List<Neighbour> expectedNeighbours = DummyNeighbourGenerator.FAVORITE_NEIGHBOURS;
        assertThat(neighbours, IsIterableContainingInAnyOrder.containsInAnyOrder(expectedNeighbours.toArray()));
    }

    @Test
    public void updateFavoriteNeighbourWithSuccess() {
        Neighbour neighbourToSetToFavorite = service.getNeighbours().get(0);
        // Set to favorite
        service.updateFavoriteNeighbour(neighbourToSetToFavorite);
        assertTrue(service.getFavoriteNeighbours().contains(neighbourToSetToFavorite));
        assertEquals(++FAVORITE_ITEMS_COUNT, service.getFavoriteNeighbours().size());
        // Unset to favorite
        service.updateFavoriteNeighbour(neighbourToSetToFavorite);
        assertFalse(service.getFavoriteNeighbours().contains(neighbourToSetToFavorite));
        assertEquals(--FAVORITE_ITEMS_COUNT, service.getFavoriteNeighbours().size());
    }
}
