package com.openclassrooms.entrevoisins.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class NeighbourTest {
    private Neighbour mNeighbour;

    public NeighbourTest() {
        this.mNeighbour = new Neighbour(2,
                "Caroline",
                "http://i.pravatar.cc/150?u=a042581f4e29026704d");
    }

    @Test
    public void givenValidSocial_whenInstatiateNeighbour_thenGetValidSocial() {
        this.mNeighbour.setSocial("https://www.facebook.com/caroline.matis");
        assertTrue("https://www.facebook.com/caroline.matis".equals(this.mNeighbour.getSocial()));
    }

    @Test
    public void givenInvalidSocial_whenInstatiateNeighbour_thenDoNotChangeSocial() {
        String expected = this.mNeighbour.getSocial();
        this.mNeighbour.setSocial("caroline.matis");
        assertTrue(expected.equals(this.mNeighbour.getSocial()));
    }
}