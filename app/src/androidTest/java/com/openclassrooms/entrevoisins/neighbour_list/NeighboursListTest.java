
package com.openclassrooms.entrevoisins.neighbour_list;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import com.openclassrooms.entrevoisins.R;
import com.openclassrooms.entrevoisins.ui.neighbour.NeighbourActivity;
import com.openclassrooms.entrevoisins.ui.neighbour_list.ListNeighbourActivity;
import com.openclassrooms.entrevoisins.utils.DeleteViewAction;
import com.openclassrooms.entrevoisins.utils.FavoriteViewAction;

import org.hamcrest.Matcher;
import org.hamcrest.core.AllOf;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.hasMinimumChildCount;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.openclassrooms.entrevoisins.utils.RecyclerViewItemCountAssertion.withItemCount;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.IsNull.notNullValue;



/**
 * Test class for list of neighbours
 */
@RunWith(AndroidJUnit4.class)
public class NeighboursListTest {

    // This is fixed
    private static int ITEMS_COUNT = 12;
    private static int FAVORITE_ITEMS_COUNT = 3;
    private static String NEIGHBOUR_NAME = "Caroline";

    private ListNeighbourActivity mActivity;

    @Rule
    public ActivityTestRule<ListNeighbourActivity> mActivityRule =
            new ActivityTestRule(ListNeighbourActivity.class);

    @Before
    public void setUp() {
        mActivity = mActivityRule.getActivity();
        assertThat(mActivity, notNullValue());
    }

    /**
     * We ensure that our recyclerview is displaying at least on item
     */
    @Test
    public void myNeighboursList_shouldNotBeEmpty() {
        // First scroll to the position that needs to be matched and click on it.
        onView(AllOf.allOf(ViewMatchers.withId(R.id.list_neighbours), isDisplayed()))
                .check(matches(hasMinimumChildCount(1)));
    }

    /**
     * When we delete an item, the item is no more shown
     */
    @Test
    public void myNeighboursList_deleteAction_shouldRemoveItem() {
        // Given : We remove the element at position 2
        onView(AllOf.allOf(ViewMatchers.withId(R.id.list_neighbours), isDisplayed()))
                .check(withItemCount(ITEMS_COUNT));
        // When perform a click on a delete icon
        onView(AllOf.allOf(ViewMatchers.withId(R.id.list_neighbours), isDisplayed()))
                .perform(RecyclerViewActions.actionOnItemAtPosition(1, new DeleteViewAction()));
        // Then : the number of element is 11
        onView(AllOf.allOf(ViewMatchers.withId(R.id.list_neighbours), isDisplayed()))
                .check(withItemCount(ITEMS_COUNT-1));
    }

    /**
     * When we click on an item, the neighbour activity is display
     */
    @Test
    public void myNeighboursList_displayAction_shouldDisplayNeighbour() {
        // When perform a click start neighbour activity
        Intents.init();
        onView(AllOf.allOf(ViewMatchers.withId(R.id.list_neighbours), isDisplayed()))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        intended(hasComponent(NeighbourActivity.class.getName()));
    }

    /**
     * When we click on an item, the neighbour activity is display with valid text
     */
    @Test
    public void myNeighboursList_displayAction_shouldDisplayNeighbourWithValidText() {
        // When perform a click start neighbour activity and check valid name
        onView(AllOf.allOf(ViewMatchers.withId(R.id.list_neighbours), isDisplayed()))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        onView(withId(R.id.name_tv))
                .check(matches(withText(NEIGHBOUR_NAME)));
    }

    /**
     * When we unset to favorites an item, the item is unset to favorites
     */
    @Test
    public void myNeighboursFavoriteList_favoriteAction_shouldRemoveAnItemOnMyNeighboursFavoriteList() {
        Matcher<View> matcher = allOf(withText(R.string.tab_favorites_title),
                isDescendantOfA(withId(R.id.tabs)));
        onView(matcher).perform(click());

        onView(allOf(withId(R.id.list_neighbours), isDisplayed()))
                .check(withItemCount(FAVORITE_ITEMS_COUNT));
        onView(AllOf.allOf(withId(R.id.list_neighbours), isDisplayed()))
                .perform(actionOnItemAtPosition(0, new FavoriteViewAction()));
        onView(allOf(withId(R.id.list_neighbours), isDisplayed()))
                .check(withItemCount(--FAVORITE_ITEMS_COUNT));
    }

    /**
     * When we set to favorites an item, the item is set to favorites
     */
    @Test
    public void myNeighboursList_favoriteAction_shouldAddAnItemOnMyNeighboursFavoriteList() {
        onView(AllOf.allOf(withId(R.id.list_neighbours), isDisplayed()))
                .perform(actionOnItemAtPosition(0, new FavoriteViewAction()));

        Matcher<View> matcher = allOf(withText(R.string.tab_favorites_title),
                isDescendantOfA(withId(R.id.tabs)));
        onView(matcher).perform(click());

        onView(allOf(withId(R.id.list_neighbours), isDisplayed()))
                .check(withItemCount(++FAVORITE_ITEMS_COUNT));
    }
}